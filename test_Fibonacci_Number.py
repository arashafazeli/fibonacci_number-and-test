import Fibonacci_Number

def test_fibonacci():
    assert Fibonacci_Number.fibonacci(9) == 34
    assert Fibonacci_Number.fibonacci(12) == 144
def test_fibonacci_type():    
    assert type(Fibonacci_Number.fibonacci(9)) is int